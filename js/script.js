class Employee {
    constructor(options) {
        this.name = options.name
        this.age  = options.age
        this.salary = options.salary
    }

    get name () {
        return this._name
    };

    set name (newName) {
        this._name = newName
    };



    get age () {
        return this._age
    };

    set age (newAge) {
        this._age = newAge
    };



    get salary () {
        return this._salary
    };

    set salary (newSalary) {
        this._salary = newSalary
    };
}

class Programmer extends Employee {
    constructor(options) {
        super(options)
        this.lang = options.lang
    }

    get salary () {
        return this._salary * 3
    }

    set salary (newSalary) {
        this._salary = newSalary
    };

}


const jsProg = new Programmer({
    name: 'Nicolas',
    age: 21,
    salary: 5000,
    lang: 'JS'
})

const phpProg = new Programmer({
    name: 'Kitty',
    age: 25,
    salary: 8000,
    lang: 'PHP'
})

const pythonProg = new Programmer({
    name: 'Fred',
    age: 32,
    salary: 12000,
    lang: 'Python'
})

console.group("Programmers")

console.log(jsProg);
console.log(phpProg);
console.log(pythonProg);

console.groupEnd()

console.group("Programmer's salary")

console.log('Nicolas -', jsProg.salary)
console.log('Kitty -',phpProg.salary)
console.log('Fred -', pythonProg.salary)

console.groupEnd()

